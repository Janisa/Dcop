#!/bin/bash
ROOTPATH=$(pwd)

#1.编译lib库
cd ${ROOTPATH}/os && make $1 &&cd ${ROOTPATH}

mkdir -p ${ROOTPATH}/out/bin
mkdir -p ${ROOTPATH}/out/lib
mkdir -p ${ROOTPATH}/out/inc
mkdir -p ${ROOTPATH}/out/inc/core
mkdir -p ${ROOTPATH}/out/inc/secure

#2.制作存档目录
cp -rfv  ${ROOTPATH}/os/secure/lib/libsecurec.so ${ROOTPATH}/out/lib
cp -rfv  ${ROOTPATH}/os/core/libjanisa.so        ${ROOTPATH}/out/lib
cp -rf   ${ROOTPATH}/os/core/inc/*               ${ROOTPATH}/out/inc/core
cp -rf   ${ROOTPATH}/os/secure/include/*         ${ROOTPATH}/out/inc/secure

#3编译demo
cd ./demo
make $1
cp -rfv ./demo ../out/bin

#4.打包发布
cd ../out

if [ "$1" != "debug" ];then
strip ./lib/* ./bin/*
fi

tar zcvf  Dcop.tar.gz ./bin ./lib