#include <objKernel.h>

int main()
{
#if !defined(_MSC_VER) && !defined(VS_LINUX_DBG)
    if (fork() == 0)
    {
        setsid();
#else
    {
#endif
        objKernel *obj = new objKernel;
        obj->Entry();
        delete obj;
    }
    return 0;
}
