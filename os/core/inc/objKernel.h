#ifndef __OBJKERNEL_H__
#define __OBJKERNEL_H__
#include <base.h>
#include <log.h>

typedef struct _ObjModule
{
    objbase *obj;
    int refCount;
    int id;
    char name[OBJ_NAME_MAX_LEN];
    _ObjModule(objbase *obj, const char *name, int key = 0) : refCount(0)
    {
        this->obj = obj;
        id = key;
        strncpy_s(this->name, OBJ_NAME_MAX_LEN,
                  name, OBJ_NAME_MAX_LEN - 1);
    }
} ObjModule;

class objKernel : public objbase
{
    DISABLE_COPY_CLASS(objKernel);

public:
    objKernel();
    ~objKernel();
    /*普通对象查询*/
    objbase *InterFace(const char *pzName);
    objbase *InterFace(int key);
    /*引用对象查询*/
    objbase *Query(int key);
    /*引用对象释放*/
    void Release(int key);
    void Entry();
    void dump(TextTable &t);
    void Init(VoidEntryFunc EntryFunc);
    void Reg(const char *pzName, void *obj, int id);
    STATUS UnReg(int id);
    void Welcome(int fd = 0);

    /*
    [0~1] <主版本号>.[2]<次版本号>.[3]<修订版本号>
    */
    DWORD VersionGet() const;

private:
    std::map<const char *, std::shared_ptr<ObjModule>> m_objList;
    VoidEntryFunc m_EntryFunc = nullptr;
    std::mutex m_lock;
};
extern objKernel *g_objKernel;

#endif
