#ifndef __LOG_H__
#define __LOG_H__
#include <logFile.h>
#ifdef __cplusplus
extern "C"
{
#endif
    int logInitial();
    void logRelease();
    LogFile *logModuleGet(UINT m);
    int logModuleReg(const char *, int fileCnt = 10,
                     int fileSize = 10, int level = LOG_LVL_INFO);
    int logWrite(UINT module, int level, const char *file,
                 const char *func, int line, const char *format, ...);
    void logHexdump(UINT module, const char *name, uint8_t width,
                    const char *buf, uint16_t size);
    int mkdirPath(const char *);
#ifdef __cplusplus
}
#endif

#define LVOS_Log(level, ...)                            \
    logWrite(MODULE_ALL, level, __FILE__, __FUNCTION__, \
             __LINE__, ##__VA_ARGS__)
#define LOG_DBG(format, ...) \
    LVOS_Log(LOG_LVL_DEBUG, format, ##__VA_ARGS__)
#define LOG_INFO(format, ...) \
    LVOS_Log(LOG_LVL_INFO, format, ##__VA_ARGS__)
#define LOG_WARN(format, ...) \
    LVOS_Log(LOG_LVL_WARN, format, ##__VA_ARGS__)
#define LOG_ERROR(format, ...) \
    LVOS_Log(LOG_LVL_ERROR, format, ##__VA_ARGS__)
#define LOG_FATAL(format, ...) \
    LVOS_Log(LOG_LVL_FATAL, format, ##__VA_ARGS__)
#define LOG_TRACE(format, ...) \
    LVOS_Log(LOG_LVL_TRACE, format, ##__VA_ARGS__)
#define LVOS_DUMP(name, width, buf, siez) \
    logHexdump(MODULE_ALL, name, width, buf, siez)
#ifdef LOG_ASSERT_ENABLE
extern void (*log_assert_hook)(const char *expr, const char *func, size_t line);
#define LVOS_ASSERT(EXPR)                                                \
    if ((EXPR))                                                          \
    {                                                                    \
        if (log_assert_hook == NULL)                                     \
        {                                                                \
            LVOS_Log(LOG_LVL_FATAL, "(%s) has assert failed at %s:%ld.", \
                     #EXPR, __FUNCTION__, __LINE__);                     \
            while (1)                                                    \
                ;                                                        \
        }                                                                \
        else                                                             \
        {                                                                \
            log_assert_hook(#EXPR, __FUNCTION__, __LINE__);              \
        }                                                                \
    }
#else
#define LVOS_ASSERT(EXPR) (assert((EXPR)))
#endif

#define PARAM_COND_CHECK_RET_VAL(cond, _retVal) \
    do                                          \
    {                                           \
        if ((cond))                             \
            return _retVal;                     \
    } while (0)

#endif
