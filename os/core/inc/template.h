#ifndef __TEMPLATE_H__
#define __TEMPLATE_H__
namespace OS
{
    template <class T>
    inline typename T::iterator find(const char *pzName, T &list)
    {
        for (typename T::iterator iter = list.begin();
             iter != list.end(); iter++)
        {
            if (
#ifndef _MSC_VER
                strcasecmp
#else
                _stricmp
#endif
                (pzName, iter->first) == 0)
            {
                return iter;
            }
        }
        return list.end();
    }
    bool equal(const char *s1, const char *s2);
    bool equal(const char *s1, const char *s2, int max);
}; // namespace OS
namespace std
{
    template <class T>
    inline std::string to_hexstring(T _Val)
    { // convert double to string
#define _Len 32
        std::string _Str(_Len, '\0');
        _Str.resize(sprintf_s(&_Str[0], _Len, "0x%x\0", _Val));
        return _Str;
    }

    template <typename Target>
    inline Target to_int(std::string _Val)
    {
        return atoi(_Val.c_str());
    }
    template <typename Target>
    inline Target to_uint(std::string _Val)
    {
        return atol(_Val.c_str());
    }
};
#endif