#ifndef __TELNET_H__
#define __TELNET_H__
#include <clibase.h>
enum EXIT_TYPE
{
    EXIT_INVALID,
    USER_EXIT,
    SYS_EXIT
};
class Telnet : public objbase
{
private:
#ifndef _MSC_VER
    INT16 m_port;
    int m_listenFd;
    EXIT_TYPE m_isExit;
    int m_epfd;
#endif
    std::map<int, int> m_servList;
    cliMgr *m_cliMgr = nullptr;
    std::mutex m_synclock;

public:
    Telnet(INT16 port = 2323);
    ~Telnet();
    void PostMsg(std::ostringstream &oss);
    virtual int postInit();
    void Process();
};
int Report(Rspmsg &outMessage);
int Report(Rspmsg &outMessage, TextTable &t);
#endif