#ifndef __THREAD_POOL_H__
#define __THREAD_POOL_H__

#include <base.h>

#define MAX_THREAD_NUM 32

//线程池,可以提交变参函数或拉姆达表达式的匿名函数执行,可以获取执行返回值
//不支持类成员函数, 支持类静态成员函数或全局函数,Opteron()函数等
class threadpool
{
    using Task = std::function<void()>;

private:
    // 线程池
    std::vector<std::thread> pool;
    // 任务队列
    std::queue<Task> tasks;
    // 同步
    std::mutex m_lock;
    // 条件阻塞
    std::condition_variable cv_task;
    // 是否关闭提交
    std::atomic<bool> stoped;
    //空闲线程数量
    std::atomic<int> idlThrNum;

public:
    inline threadpool(int size = 1) : stoped{false}
    {
        idlThrNum = (size < 1 ? 1 : (size > MAX_THREAD_NUM ? MAX_THREAD_NUM : size));
        for (size = 0; size < idlThrNum; ++size)
        { //初始化线程数量
            pool.emplace_back(
                [this] { // 工作线程函数
                    while (!this->stoped)
                    {
                        std::function<void()> task;
                        { // 获取一个待执行的 task
                            // unique_lock 相比 lock_guard 的好处是：可以随时 unlock() 和 lock()
                            std::unique_lock<std::mutex> lock{this->m_lock};
                            this->cv_task.wait(lock,
                                               [this] {
                                                   return this->stoped.load() ||
                                                          !this->tasks.empty();
                                               }); // wait 直到有 task
                            if (this->stoped && this->tasks.empty())
                                break;
                            task = std::move(this->tasks.front()); // 取一个 task
                            this->tasks.pop();
                        }
                        idlThrNum--;
                        task();
                        idlThrNum++;
                    }
                    idlThrNum--; /* 退出操作*/
                });
        }
    }
    inline ~threadpool()
    {
        stoped.store(true);
        cv_task.notify_all(); // 唤醒所有线程执行
        for (std::thread &thread : pool)
        {
            //thread.detach(); // 让线程“自生自灭”
            if (thread.joinable())
                thread.join(); // 等待任务结束， 前提：线程一定会执行完
        }
        while (idlThrNum > 0)
        {
            // 等待所有线程退出
        }
    }

public:
    // 提交一个任务
    // 调用.get()获取返回值会等待任务执行完,获取返回值
    // 有两种方法可以实现调用类成员，
    // 一种是使用   bind： .commit(std::bind(&Dog::sayHello, &dog));
    // 一种是用 mem_fn： .commit(std::mem_fn(&Dog::sayHello), &dog)
    template <class F, class... Args>
    // int commit(F &&f, Args &&... args)->std::future<decltype(f(args...))>
    int commit(F &&f, Args &&... args)
    {
        if (stoped.load()) // stop == true
            return -1;

        using RetType = decltype(f(args...)); // typename std::result_of<F(Args...)>::type, 函数 f 的返回值类型
        auto task = std::make_shared<std::packaged_task<RetType()>>(
            std::bind(std::forward<F>(f), std::forward<Args>(args)...));
        // std::future<RetType> future = task->get_future();
        // {                                             // 添加任务到队列
        std::lock_guard<std::mutex> lock{m_lock}; //对当前块的语句加锁  lock_guard 是 mutex 的 stack 封装类，构造的时候 lock()，析构的时候 unlock()
        tasks.emplace(
            [task]() { // push(Task{...})
                (*task)();
            });
        // }
        cv_task.notify_one(); // 唤醒一个线程执行

        return 0;
    }

    //空闲线程数量
    int idlCount() { return idlThrNum; }
};

#endif
