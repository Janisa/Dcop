#ifndef __OBJBASE_H__
#define __OBJBASE_H__
#include <cargclist.h>
#include <frameworkmgr.h>
#include <cmdline.h>
#include <table.h>
#define RSP_MESSAGE_LEN 1024
#define OBJ_NAME_MAX_LEN 16
//���ø���
#define DISABLE_COPY_CLASS(cls) \
private:                        \
    cls(const cls &) = delete;  \
    cls &operator=(const cls &) = delete;

using StringMap = std::map<std::string, std::string>;
class Rspmsg
{
private:
    std::vector<StringMap> list;
    std::string name;

public:
    std::vector<StringMap> &operator()() { return list; }
    size_t size() const { return list.size(); }
    std::vector<StringMap>::iterator begin() { return list.begin(); }
    std::vector<StringMap>::iterator end() { return list.end(); }
    void addItem(std::string key, std::string value)
    {
        StringMap val;
        val[key] = value;
        list.push_back(std::move(val));
    }
    void setName(std::string val) { name = std::move(val); }
    std::string &getName() { return name; }
    void push_back(StringMap &v) { list.push_back(v); }
};

using ArgcList = parser::parameter;
typedef struct argc
{
    ArgcList argclist;
    int fd;
    argc(int fd = 0)
    {
        this->fd = fd;
    }
} ARGC;
using Parameter = std::shared_ptr<ARGC>;

#ifdef __cplusplus
extern "C"
{
#endif
    typedef LONG (*Printfun)(int fd, const char *format, ...);
    LONG LVOS_Printf(int fd, const char *format, ...);
#ifdef __cplusplus
}
#endif

void LVOS_Write(int fd, TextTable &t);
void LVOS_Write(int fd, std::ostringstream &oss);
void LVOS_Write(int fd, std::string &str);
class objbase
{
private:
    std::atomic<int> m_debug;
    DISABLE_COPY_CLASS(objbase);

public:
    std::mutex m_synclock;

public:
    objbase() : m_debug(0) {}
    virtual ~objbase() {}
    virtual int postInit() { return 0; };
    virtual void dump(TextTable &t){};
    virtual int GetDebug() { return m_debug; }
    virtual int SetDebug(int value) { return (m_debug = value); }
    virtual int Process(ArgcList &message, Rspmsg &outmessage,
                        int iModule, int iCmd) { return SUCCESS; };
    virtual void Reg(const char *pzName, void *obj, int id){};
    virtual void preInit(){};
};

#define PROCESS_BEGIN(cmd)                                        \
    LOG_TRACE("Receive Command: mid(%d) cid(%d)", iModule, iCmd); \
    std::lock_guard<std::mutex> lock{m_synclock};                 \
    int iRet = 0;                                                 \
    switch (cmd)                                                  \
    {

#define PROCESS_CALL(cmd, func)                            \
    case cmd:                                              \
        iRet = (func)(message, outmessage, iModule, iCmd); \
        break;

#define PROCESS_END() \
    default:          \
        iRet = -1;    \
        break;        \
        }             \
        return iRet;

#endif
