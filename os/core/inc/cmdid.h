#ifndef __CMDID_H__
#define __CMDID_H__
#include <type.h>
#include <string>
enum CMD_ID
{
    CMD_BASE_ID = 0,
    CMD_SYS_HELP,
    CMD_SYS_DUMP,
    CMD_SYS_EXIT,
    CMD_SYS_SHUT_DOWN,
    CMD_SYS_ERR_CODE,
    CMD_SET_LOG_COLOR,
    CMD_GET_LOG_COLOR,
    CMD_GET_LOG_LEVEL,
    CMD_SET_LOG_LEVEL,
    CMD_GET_LOG_TRACE,
    CMD_SET_LOG_TRACE,
    CMD_MSG_REPORT,
    CMD_MSG_TIMER,
    CMD_MSG_DB_INIT,
    CMD_MSG_DB_SAVE,
    CMD_SYS_VER,
    CMD_MORE_TIPS,
    CMD_SYS_KEY,
    CMD_BASE_NUM,
};

enum MODULE_ID
{
    MODULE_ALL,
    MODULE_KERNEL,
    MODULE_LOG_MGR,
    MODULE_NOTIFY,
    MODULE_CLI,
    MODULE_TASK,
    MODULE_TELNET,
    MODULE_TIMER,
    MODULE_WORK_TASK,
    MODULE_DB_MGR,
    MODULE_LOGGER,
    MODULE_BASE_END,
};

enum ERR_CODE
{
    ERROR = -1,           //Operation failure
    SUCCESS,              //Operation is successful
    ERR_DISPATCH,         //Command distribution failed
    ERR_NOTIFY,           //Message publishing failed
    ERR_REPORT,           //Failed to report the message
    ERR_PARAM,            //Parameter processing error
    ERR_UNKOWN,           //Unregistered error
    ERR_NOT_SUPPORT,      //Feature not supported
    ERR_MSG_PARAM,        //Message parameter is invalid
    ERR_MSG_FIELD,        //Invalid message field
    ERR_MODULE_REF_CNT,   //Module has references
    ERR_MODULE_NOT_EXIST, //The module does not exist
};
enum REVALUE
{
    KEY_VAL_INVALID = -1,
    KEY_VAL_FALSE,
    KEY_VAL_TRUE,
    KEY_VAL_ON,
    KEY_VAL_OFF,
    /* output log's level */
    LOG_LVL_FATAL,
    LOG_LVL_ERROR,
    LOG_LVL_WARN,
    LOG_LVL_INFO,
    LOG_LVL_DEBUG,
    LOG_LVL_TRACE,
    LOG_LVL_NUM,
    KEY_VAL_FATAL = LOG_LVL_FATAL,
    KEY_VAL_ERROR,
    KEY_VAL_WARN,
    KEY_VAL_INFO,
    KEY_VAL_DEBUG,
    KEY_VAL_TRACE,
    KEY_VAL_CLI = MODULE_CLI,
    KEY_VAL_KERNEL = MODULE_KERNEL
};
typedef struct
{
    int id;
    const char *name;
    const char *desc;
} ERR_CODE_INFO;
typedef struct
{
    const char *key;
    int value;
    int reValue;
} KEY_MAP;

void ErroReg(ERR_CODE_INFO *err, int count);
ERR_CODE_INFO *ErrGet(int);
void KeyReg(KEY_MAP *err, int count);
int ValueGet(std::string);
int RevalueGet(std::string key); // 反向查找值 eg: ON->2
std::string KeyGet(int value);
std::string KeyByReValueGet(int reValue); // 反向查找key值 eg: 2->ON
std::string KeyByNameGet(std::string name);
#endif