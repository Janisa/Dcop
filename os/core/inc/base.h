#ifndef __BASE_H__
#define __BASE_H__

/* type define */
#include <cmdid.h>

/* C++ standard library */
#include <iostream>
#include <sstream>
#include <queue>
#include <algorithm>        // all_of, find, for_each transform
#include <cassert>          // assert
#include <cstddef>          // nullptr_t, ptrdiff_t, size_t
#include <functional>       // hash, less
#include <initializer_list> // initializer_list
#include <iosfwd>           // istream, ostream
#include <iterator>         // random_access_iterator_tag inserter, front_inserter, end
#include <memory>           // unique_ptr
#include <numeric>          // accumulate
#include <string>           // string, stoi, to_string
#include <utility>          // declval, forward, move, pair, swap pair, declval
#include <vector>           // vector
#include <array>            // array
#include <forward_list>     // forward_list
#include <map>              // map
#include <tuple>            // tuple, make_tuple
#include <type_traits>      // is_arithmetic, is_same, is_enum, underlying_type, is_convertible
#include <unordered_map>    // unordered_map
#include <valarray>         // valarray
#include <exception>        // exception
#include <stdexcept>        // runtime_error
#if __cplusplus <= 201703L
#include <ciso646> // and, not, or
#endif
#include <list>
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
#include <future>
#include <fstream>
#include <iomanip>
#include <chrono>
#include <typeinfo>
#include <cstring>
#include <cstdlib>

/* C standard library*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#ifndef _MSC_VER
#include <dirent.h>
#include <unistd.h>
#include <sys/times.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <pthread.h>
#include <ucontext.h>
#include <execinfo.h>
#include <sys/wait.h>
#include <sys/resource.h>
#include <sys/sysinfo.h>
#include <utime.h>
#include <sys/vfs.h>
#include <arpa/inet.h>
#include <sys/syscall.h>
#include <sys/prctl.h>
#include <cxxabi.h>
#else
#include <direct.h>
#include <sys/timeb.h>
#endif

/* Huawei internal security library for C character operation */
#include <securec.h>

typedef std::function<void(void)>   VoidEntryFunc;
typedef std::function<void(void *)> FunEntry;

#ifdef __cplusplus
extern "C"
{
#endif
#ifdef linux
#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)
#else
#define likely(x) (x)
#define unlikely(x) (x)
#endif
#ifdef __cplusplus
}
#endif
#endif