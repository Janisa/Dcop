#ifndef _MACRO_DEFINE__
#define _MACRO_DEFINE__

typedef signed char         INT8;
typedef signed short        INT16;
typedef signed int          INT32;
typedef signed long         LONG;
typedef signed long long    INT64;
typedef unsigned char       UINT8;
typedef unsigned short      UINT16;
typedef unsigned int        UINT32;
typedef unsigned long long  UINT64;
typedef unsigned char       UCHAR;
typedef unsigned short      USHORT;
typedef unsigned int        UINT;
typedef unsigned long       ULONG;
typedef int                 BOOL;
typedef int                 ARGINT;
typedef void                VOID;
typedef int                 STATUS;
typedef unsigned char       BYTE;
typedef unsigned long       DWORD;
typedef unsigned short      WORD;
typedef int                 INT;

#define FALSE               0
#define TRUE                1
#define	IMPORT	            extern
#define	LOCAL	            static
#define RET_OK              0
#define RET_ERR             -1
#define RET_VOID

#define RET_EXCEPTIONAL    128 
#define RET_TIMEOUT        129  
#define RET_INDIDE_ERR     130  
#define RET_STAT_D         131 

#define STAT_PATH_LEN      64
#define STAT_BUFFER_LEN    288
#define TV_USEC_VALUE      200
#define INVALUE_INAVLE     0xffffffffffffffffUL
#define MAX_PATH           1024
#define UNUSED(x)          (void)(x) //avoid gcc warning: unused variable
#define ARRAY_SIZE(arr)    (sizeof(arr) / sizeof(arr[0]))
#define MAX(x, y) (((x) < (y)) ? (y) : (x))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#endif
