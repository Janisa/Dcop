#ifndef NOTIFIER_H
#define NOTIFIER_H
#include <objKernel.h>
typedef struct _AsyncArgc
{
    Parameter msg;
    int iModule;
    int iCmd;
    void *m_ptr;
    _AsyncArgc(Parameter objmsg, int module, int cmd, void *parm = nullptr)
    {
        iModule = module;
        iCmd = cmd;
        m_ptr = parm;
        msg = objmsg;
    }

} AsyncArgc;
typedef struct regNotify
{
    objbase *obj;
    char name[OBJ_NAME_MAX_LEN];
    regNotify(objbase *_obj, const char *_name)
    {
        this->obj = _obj;
        (void)snprintf(this->name, OBJ_NAME_MAX_LEN, "%s", _name);
    }
    ~regNotify()
    {
        this->obj = nullptr;
    }
} REGNOTIFY;
class Cnotify : public objbase
{
    DISABLE_COPY_CLASS(Cnotify);

public:
    Cnotify();
    virtual ~Cnotify();
    void UnRegReceiver(int iModule);
    void NotifyA(Parameter message, Rspmsg &outmessage, int iModule, int iCmd, bool sync = false);
    void RegReceiver(int iModule, objbase *_obj, const char *_name);
    int Notify(ArgcList &message, Rspmsg &outmessage, int iModule, int iCmd);
    void dump(TextTable &t);
    void AsyncProc();
    int postInit();
    int AsyncNotify(Parameter message,
                    int iModule,
                    int iCmd, void *parm = nullptr);

private:
    std::map<int, std::shared_ptr<REGNOTIFY>> observerList;
    std::mutex m_reglock;
    // 任务队列
    std::queue<std::shared_ptr<AsyncArgc>> tasks;
    // 同步
    std::mutex m_lock;
    // 条件阻塞
    std::condition_variable cv_task;
    // 是否关闭提交
    std::atomic<bool> stoped;
    //空闲线程数量
    std::atomic<int> idlThrNum;
};

#endif // NOTIFIER_H
