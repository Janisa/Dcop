#ifndef __OBJTASK_H__
#define __OBJTASK_H__

#include <log.h>
class objPara
{
public:
    objPara(void *obj) { m_para = obj; }
    ~objPara() {}
    void *GetPara() { return m_para; }

private:
    void *m_para;
};
class objTask
{
    DISABLE_COPY_CLASS(objTask);

public:
    objTask(const char *pzName, FunEntry func, void *obj);
    inline const char *Name() { return name; }
    inline UINT64 GetId() { return tId; }
    inline void SetId(UINT64 id) { tId = id; }
    inline void Run() { m_func(m_objPara); }
    ~objTask();

private:
    UINT64 tId = 0;
    char name[OBJ_NAME_MAX_LEN] = {0};
    FunEntry m_func;
    void *m_objPara;
};

class objTaskMgr : public objbase
{

public:
    int addObj(std::shared_ptr<objTask> obj);
    void delObj(UINT64 id);
    ~objTaskMgr();
    objTaskMgr();
    void dump(TextTable &t);

private:
    objTaskMgr(objTaskMgr &) = delete;
    const objTaskMgr &operator=(const objTaskMgr &) = delete;
    std::map<UINT64, std::shared_ptr<objTask>> m_objlist;
    std::mutex m_lock;
};

#define CREATE_OBJTASK(objTaskName, func, pObjPara) \
    objTaskEntry(objTaskName, func, pObjPara, __FILE__, __LINE__)

STATUS objTaskEntry(const char *objTaskName,
                    FunEntry func, void *arg,
                    const char *file,
                    int len);

class workQ : public objbase
{

private:
    using Task = VoidEntryFunc;
    workQ(workQ &) = delete;
    const workQ &operator=(const workQ &) = delete;
    // 任务队列
    std::queue<Task> tasks;
    // 同步
    std::mutex m_lock;
    // 条件阻塞
    std::condition_variable cv_task;
    // 是否关闭提交
    std::atomic<bool> stoped;
    int sysShutdown(ArgcList &message, Rspmsg &outmessage,
                    int iModule, int iCmd);

public:
    workQ();
    ~workQ();
    void dump(TextTable &t);
    int postInit();
    void Proc();
    int Process(ArgcList &message, Rspmsg &outmessage, int iModule, int iCmd);
    STATUS commit(Task job);
};
STATUS jobAdd(VoidEntryFunc fun);
#endif
