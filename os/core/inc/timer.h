#ifndef __TIMERTASK_H__
#define __TIMERTASK_H__
#include <objbase.h>

class TimerManager : public objbase
{
private:
    void *m_timer;

private:
    DISABLE_COPY_CLASS(TimerManager);

    int sysShutdown(ArgcList &message, Rspmsg &outmessage,
                    int iModule, int iCmd);

public:
    TimerManager();
    ~TimerManager();
    int Process(ArgcList &message, Rspmsg &outmessage,
                int iModule, int iCmd);
    int postInit();
};

#endif