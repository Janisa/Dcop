#ifndef __OS_CLIBASE__
#define __OS_CLIBASE__
#include <cnotify.h>
#include <cmdline.h>

typedef struct CMD_DESC
{
    const char *name;         //命令本身的字符串
    int cmdMId;               // 命令所在模块的id
    int cmdId;                // 命令的id
    bool sync;                // 命令为同步 true
    void (*func)(ArgcList &); // 命令参数信息
} CMD_DESC;
class cliMgr : public objbase
{
    DISABLE_COPY_CLASS(cliMgr);

private:
    std::map<std::string, CMD_DESC *> m_cmdList;
    std::mutex m_reglock;
    Cnotify *m_Cnotify = nullptr;
    int Dispatch(Parameter message, CMD_DESC *pcmdObj);
    CMD_DESC *FindModule(std::string);
    void dump(TextTable &t);
    int Process(ArgcList &message, Rspmsg &outmessage, int iModule, int iCmd);
    int Ver(ArgcList &message, Rspmsg &outmessage, int iModule, int iCmd);
    int Help(ArgcList &message, Rspmsg &outmessage, int iModule, int iCmd);
    int Dump(ArgcList &message, Rspmsg &outmessage, int iModule, int iCmd);
    int MoreTips(ArgcList &message, Rspmsg &outmessage, int iModule, int iCmd);
    int ErrList(ArgcList &, Rspmsg &, int, int);
    int KeyList(ArgcList &, Rspmsg &, int, int);
    int Report(Rspmsg &outMessage, int fd);

public:
    cliMgr();
    ~cliMgr();
    int postInit();
    void Welcome(int fd = 0);
    int RegCmd(CMD_DESC *list, int count);
    int Process(int fd, char *cmdbuffer, ULONG len, int *isExit);
};
#endif
