#ifndef __LOGFILE_H__
#define __LOGFILE_H__
#include <base.h>
#include <objbase.h>

#define LOG_FILENAME_LEN 32
#define LOG_LEVEL_MASK (LOG_LVL_FATAL | LOG_LVL_ERROR | LOG_LVL_WARN | LOG_LVL_INFO | LOG_LVL_DEBUG | LOG_LVL_TRACE)
class LogFile : public objbase
{
    DISABLE_COPY_CLASS(LogFile);

public:
    LogFile(const char *, int fileCnt = 10,
            int fileSize = 10, int level = LOG_LVL_INFO);
    ~LogFile();
    int write(int level, const char *filename, const char *func,
              int line, const char *format, ...);
    int write(int level, const char *file, const char *func,
              int line, const char *format, va_list argList);
    inline int getLevel() const { return m_logLevel; }
    inline void setLevel(int level) { m_logLevel = LOG_LEVEL_MASK & level; }
    void hexdump(const char *name, uint8_t width, const char *buf, uint16_t size);
    inline int write(const char *buf, int len);

private:
    void file_rotate(void);
    bool file_retate_check(void);
    inline void reOpen();
    inline void close();
    int compress();
private:
    int m_fileCnt;
    int m_fileSzie;
    int m_logLevel;
    char m_fileName[LOG_FILENAME_LEN];
    std::mutex m_lock;
    FILE *m_file;
};
#endif