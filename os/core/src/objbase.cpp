#include <objbase.h>

LONG LVOS_Printf(int fd, const char *format, ...)
{
    char buffer[RSP_MESSAGE_LEN] = {0};
    va_list arg_ptr;
    va_start(arg_ptr, format);
    int nWrittenBytes = vsnprintf(buffer, sizeof(buffer), format, arg_ptr);
    va_end(arg_ptr);
    if (nWrittenBytes <= 0)
    {
        return nWrittenBytes;
    }
    std::ostringstream oss;
    oss << buffer;
    LVOS_Write(fd, oss);
    return nWrittenBytes;
}
void LVOS_Write(int fd, TextTable &t)
{
#ifndef _MSC_VER
    if (fd)
    {
        std::ostringstream oss;
        oss << t << std::endl;
        (void)write(fd, oss.str().c_str(), oss.str().length());
    }
    else
#endif
    {
        std::cout << t;
    }
}
void LVOS_Write(int fd, std::ostringstream &oss)
{
#ifndef _MSC_VER
    if (fd)
    {
        (void)write(fd, oss.str().c_str(), oss.str().length());
    }
    else
#endif
    {
        std::cout << oss.str();
    }
}
void LVOS_Write(int fd, std::string &str)
{
#ifndef _MSC_VER
    if (fd)
    {
        (void)write(fd, str.c_str(), str.length());
    }
    else
#endif
    {
        std::cout << str;
    }
}