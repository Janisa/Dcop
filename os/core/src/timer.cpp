#include <timer.h>
#include <objTask.h>
#include <cnotify.h>
#include <table.h>

template <typename T>
class Timer
{
private:
    bool clear = false;

public:
    void setTimeout(T function, int delay)
    {
        this->clear = false;
        std::thread t([=]()
                      {
                          if (this->clear)
                              return;
                          std::this_thread::sleep_for(
                              std::chrono::milliseconds(delay));
                          if (this->clear)
                              return;
                          function();
                      });
        t.detach();
    }
    void setInterval(T function, int interval)
    {
        this->clear = false;
        std::thread t([=]()
                      {
                          while (true)
                          {
                              if (this->clear)
                                  return;
                              std::this_thread::sleep_for(
                                  std::chrono::milliseconds(interval));
                              if (this->clear)
                                  return;
                              function();
                          }
                      });
        t.detach();
    }
    void stop()
    {
        this->clear = true;
    }
};
TimerManager::TimerManager()
{
    m_timer = new Timer<void()>;
}
TimerManager::~TimerManager()
{
    if (m_timer)
        delete reinterpret_cast<Timer<void()> *>(m_timer);
}
int TimerManager::postInit()
{
    Cnotify *obj =
        reinterpret_cast<Cnotify *>(g_objKernel->InterFace(MODULE_NOTIFY));
    if (obj)
        obj->RegReceiver(MODULE_TIMER, this, "TimerManager");

    reinterpret_cast<Timer<void()> *>(m_timer)->setInterval(
        []()
        {
            Rspmsg rspmsg;
            Parameter list = std::make_shared<ARGC>();
            Cnotify *obj = reinterpret_cast<Cnotify *>(
                g_objKernel->InterFace(MODULE_NOTIFY));
            if (obj)
                obj->NotifyA(list, rspmsg, 0, CMD_MSG_TIMER);
        },
        1000);
    return 0;
}
int TimerManager::Process(ArgcList &message, Rspmsg &outmessage,
                          int iModule, int iCmd)
{
    PROCESS_BEGIN(iCmd)
    PROCESS_CALL(CMD_SYS_SHUT_DOWN, sysShutdown)
    PROCESS_END()
}
int TimerManager::sysShutdown(ArgcList &message, Rspmsg &outmessage,
                              int iModule, int iCmd)
{
    reinterpret_cast<Timer<void()> *>(m_timer)->stop();
    return SUCCESS;
}

REG_TO_FRAMEWORK(TABLE_ONE, MODULE_KERNEL, TimerManager, MODULE_TIMER)