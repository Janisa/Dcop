#include <cnotify.h>
#include <objTask.h>
#include <table.h>
void Cnotify::RegReceiver(int iModule, objbase *obj, const char *name)
{

    std::unique_lock<std::mutex> lock{this->m_reglock};
    auto iter = observerList.find(iModule);
    if (iter == observerList.end())
    {
        observerList[iModule] = std::make_shared<REGNOTIFY>(obj, name);
    }
}
void Cnotify::UnRegReceiver(int iModule)
{
    std::unique_lock<std::mutex> lock{this->m_reglock};
    auto iter = observerList.find(iModule);
    if (iter != observerList.end())
    {
        observerList.erase(iter);
    }
}
void Cnotify::NotifyA(Parameter message, Rspmsg &outmessage,
                      int iModule,
                      int iCmd,
                      bool sync)
{
    for (auto &iter : observerList)
    {
        if (sync)
        {
            (void)iter.second->obj->Process(message->argclist, outmessage, iModule, iCmd);
        }
        else
        {
            (void)AsyncNotify(message, iter.first, iCmd, nullptr);
        }
    }
}

int Cnotify::Notify(ArgcList &message, Rspmsg &outmessage,
                    int iModule,
                    int iCmd)
{
    auto iter = observerList.find(iModule);
    if (iter != observerList.end() && iModule == iter->first)
    {
        return iter->second->obj->Process(message, outmessage, iModule, iCmd);
    }
    return -1;
}
int Cnotify::AsyncNotify(Parameter message, int iModule, int iCmd, void *parm)
{
    std::shared_ptr<AsyncArgc> pool = std::make_shared<AsyncArgc>(message, iModule, iCmd, parm);
    std::lock_guard<std::mutex> lock{m_lock};
    tasks.emplace(pool);
    cv_task.notify_one();
    return 0;
}
Cnotify::~Cnotify()
{
    stoped.store(true);
    cv_task.notify_all();
    while (idlThrNum > 0)
        ;
    observerList.clear();
}
Cnotify::Cnotify()
{
    stoped = false;
    idlThrNum = 10;
}
void Cnotify::AsyncProc()
{
    while (!this->stoped)
    {
        std::shared_ptr<AsyncArgc> message;
        {
            std::unique_lock<std::mutex> lock{this->m_lock};

            this->cv_task.wait(lock,
                               [this]
                               {
                                   return this->stoped.load() ||
                                          !this->tasks.empty();
                               });
            if (this->stoped && this->tasks.empty())
            {
                idlThrNum--;
                return;
            }
            message = this->tasks.front();
            this->tasks.pop();
        }
        idlThrNum--;
        if (message != nullptr)
        {
            Rspmsg outmessage;
            if (message->iModule == MODULE_ALL)
            {
                NotifyA(message->msg,
                        outmessage,
                        message->iModule,
                        message->iCmd,
                        true);
            }
            else
            {
                Notify(message->msg->argclist,
                       outmessage,
                       message->iModule,
                       message->iCmd);
            }
        }
        idlThrNum++;
    }
    idlThrNum--;
}
int Cnotify::postInit()
{
    for (int i = 0; i < idlThrNum; i++)
    {
        const int ibufmaxlen = 16;
        char buf[ibufmaxlen] = {0};
        (void)snprintf_s(buf, ibufmaxlen - 1, ibufmaxlen, "NotifyTask%d", i);
        CREATE_OBJTASK(
            buf, [=](void *pobjPara)
            { this->AsyncProc(); },
            nullptr);
    }

    return 0;
}
void Cnotify::dump(TextTable &t)
{
    t.add("objId").add("objPtr").add("objName").endOfRow().setTableName("Cnotify");

    for (auto &iter : observerList)
    {
        t.add(std::to_hexstring(iter.first)).add(std::to_hexstring(iter.second->obj));
        t.add(iter.second->name).endOfRow();
    }
}

REG_TO_FRAMEWORK(TABLE_ONE, MODULE_KERNEL, Cnotify, MODULE_NOTIFY)