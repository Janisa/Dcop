﻿#include <clibase.h>
#include <table.h>
#include <telnet.h>

#define DEFAULT_LOG_FILE_PATH "../log/cli.log"
static int gCliLogObjId = 0;
#define CLI_Log(level, ...)                               \
    logWrite(gCliLogObjId, level, __FILE__, __FUNCTION__, \
             __LINE__, ##__VA_ARGS__)
int cliMgr::Dispatch(Parameter message, CMD_DESC *pcmdObj)
{

    Rspmsg outMessage;
    int iRet = 0;
    if (pcmdObj->sync)
    {
        iRet = m_Cnotify->Notify(message->argclist, outMessage,
                                 pcmdObj->cmdMId, pcmdObj->cmdId);
        if (iRet == 0)
        {
            iRet = Report(outMessage, message->fd);
        }
    }
    else
    {

        iRet = m_Cnotify->AsyncNotify(message, pcmdObj->cmdMId,
                                      pcmdObj->cmdId);
    }
    return iRet;
}
int cliMgr::Process(ArgcList &message, Rspmsg &outmessage, int iModule, int iCmd)
{
    PROCESS_BEGIN(iCmd)
    PROCESS_CALL(CMD_SYS_HELP, Help)
    PROCESS_CALL(CMD_SYS_VER, Ver)
    PROCESS_CALL(CMD_MORE_TIPS, MoreTips)
    PROCESS_CALL(CMD_SYS_DUMP, Dump)
    PROCESS_CALL(CMD_SYS_KEY, KeyList)
    PROCESS_CALL(CMD_SYS_ERR_CODE, ErrList)
    PROCESS_END()
}
int cliMgr::Dump(ArgcList &message, Rspmsg &outmessage,
                 int iModule, int iCmd)
{
    TextTable t;
    objbase *pobjBase = g_objKernel->InterFace(message.get<std::string>("module").c_str());
    if (pobjBase)
    {
        pobjBase->dump(t);
    }
    else
        g_objKernel->dump(t);
    if (t.has_columns())
    {
        std::ostringstream oss;
        oss << t;
        std::map<std::string, std::string> rsp;
        rsp["Dump information"] = oss.str();
        outmessage.push_back(rsp);
    }
    return RET_OK;
}
int cliMgr::Ver(ArgcList &message, Rspmsg &outmessage,
                int iModule, int iCmd)
{
    BYTE ver[8];
    DWORD val = g_objKernel->VersionGet();
    *(DWORD *)(&ver) = val;
    std::map<std::string, std::string> rsp;
    rsp["Version"] = std::to_string(ver[0]) +
                     "." + std::to_string(ver[1]) +
                     "." + std::to_string(ver[2]);
    outmessage.push_back(rsp);
    return RET_OK;
}
int cliMgr::MoreTips(ArgcList &message, Rspmsg &outmessage,
                     int iModule, int iCmd)
{
    SetDebug(ValueGet(message.get<std::string>("status")));
    return RET_OK;
}
int cliMgr::Help(ArgcList &message, Rspmsg &outmessage,
                 int iModule,
                 int iCmd)
{
    CMD_DESC *pobj = FindModule(message.get<std::string>("cmd"));
    parser::parameter param;
    if (pobj != nullptr)
    {
        std::map<std::string, std::string> rsp;
        pobj->func(param);
        rsp["Help information"] = param.usage();
        outmessage.push_back(rsp);
    }
    else
    {
        for (auto &iter : m_cmdList)
        {
            std::map<std::string, std::string> rsp;
            rsp["Support command list"] = iter.second->name;
            outmessage.push_back(rsp);
        }
    }
    return RET_OK;
}
int cliMgr::KeyList(ArgcList &message, Rspmsg &outmessage,
                    int iModule, int iCmd)
{
    outmessage.setName("Keyword-mapp-table");
    extern std::map<std::string, KEY_MAP *> m_keyList;
    for (auto &iter : m_keyList)
    {
        std::map<std::string, std::string> rsp;
        rsp["Name"] = iter.first;
        rsp["Value"] = std::to_hexstring(iter.second->value);
        rsp["ReValue"] = std::to_hexstring(iter.second->reValue);
        outmessage.push_back(rsp);
    }
    return RET_OK;
}
int cliMgr::ErrList(ArgcList &message, Rspmsg &outmessage,
                    int iModule, int iCmd)
{
    outmessage.setName("ERR-CODE-TABLE");
    extern std::map<int, ERR_CODE_INFO *> m_errList;
    for (auto &iter : m_errList)
    {
        std::map<std::string, std::string> rsp;
        rsp["errId"] = std::to_hexstring(iter.second->id);
        rsp["Name"] = iter.second->name;
        rsp["Description"] = iter.second->desc;
        outmessage.push_back(rsp);
    }
    return RET_OK;
}
int cliMgr::Report(Rspmsg &outMessage, int fd)
{
    TextTable t(outMessage.getName());
    if (RET_OK == ::Report(outMessage, t))
    {
        std::ostringstream total;
        total << std::endl
              << t << "Tatol:" << (t.rows().size() - 1) << " record" << std::endl;
        LVOS_Write(fd, total);
    }
    return RET_OK;
}

void cliMgr::Welcome(int fd)
{
#ifndef _MSC_VER
    g_objKernel->Welcome(fd);
#endif
    std::ostringstream oss;
    oss << "\tWelcome!" << std::endl
        << "him:#";
    LVOS_Write(fd, oss);
}
int cliMgr::Process(int fd, char *cmdbuffer, ULONG len, int *isExit)
{

    CMD_DESC *pcmdObj = nullptr;
    Parameter param = std::make_shared<ARGC>(fd);
    std::string cmd;
    char *pos = nullptr;
    int iRet = RET_OK;

    pos = strchr(cmdbuffer, ' ');
    if (pos != nullptr)
        cmd.append(cmdbuffer, pos - cmdbuffer);
    else
        cmd.append(cmdbuffer, len);

    CLI_Log(LOG_LVL_INFO, "cmd:%s", cmdbuffer);

    pcmdObj = FindModule(cmd);
    if (pcmdObj != nullptr)
    {
        switch (pcmdObj->cmdId)
        {
        case CMD_SYS_EXIT:
        {
            *isExit = USER_EXIT;
            break;
        }
#ifdef _DEBUG
        case CMD_SYS_SHUT_DOWN:
        {
            *isExit = SYS_EXIT;
            break;
        }
#endif
        default:
        {
            pcmdObj->func(param->argclist);
            iRet = ERR_MSG_PARAM;
            if (param->argclist.parse_check(cmdbuffer) == 0)
                iRet = Dispatch(param, pcmdObj);
            break;
        }
        }
    }
    else if (strlen(cmdbuffer) != 0)
    {
        iRet = ERR_NOT_SUPPORT;
    }
    else
    {
    }

    std::ostringstream oss;
    if (iRet != 0)
    {
        ERR_CODE_INFO *err = ErrGet(iRet);
        LOG_WARN("Dispatch is process error:%s(%#X)", err->name, err->id);
        if (GetDebug() == KEY_VAL_ON)
            oss << param->argclist.error_full() << std::endl;
        oss << err->name << "(" << std::to_hexstring(err->id) << ")"
            << ":" << err->desc << std::endl;
    }
    oss << std::endl
        << "him:#";
    LVOS_Write(fd, oss);
    return iRet;
}
CMD_DESC *cliMgr::FindModule(std::string cmd)
{
    auto iterCmd = m_cmdList.find(cmd);
    if (iterCmd == m_cmdList.end())
        return nullptr;
    return iterCmd->second;
}
void cliMgr::dump(TextTable &t)
{
    t.add("cmdId").add("objId").add("cmd").endOfRow().setTableName("cliMgr");

    for (auto &iter : m_cmdList)
    {
        t.add(std::to_string(iter.second->cmdId)).add(std::to_string(iter.second->cmdMId));
        t.add(iter.second->name).endOfRow();
    }
}
cliMgr::~cliMgr()
{
    m_cmdList.clear();
}
cliMgr::cliMgr()
{
    SetDebug(KEY_VAL_OFF);
}

int cliMgr::RegCmd(CMD_DESC *list, int num)
{
    int count = 0;
    std::unique_lock<std::mutex> lock{m_reglock};
    for (int index = 0; index < num; index++)
    {
        auto iter = m_cmdList.find(list[index].name);
        if (iter != m_cmdList.end())
        {
            continue;
        }
        m_cmdList[list[index].name] = &list[index];
        count++;
    }

    return count;
}
int cliMgr::postInit()
{
    m_Cnotify = reinterpret_cast<Cnotify *>(g_objKernel->Query(MODULE_NOTIFY));
    m_Cnotify->RegReceiver(MODULE_CLI, this, "cliMgr");
    gCliLogObjId = logModuleReg(DEFAULT_LOG_FILE_PATH, 10, 10, LOG_LVL_INFO);
    LOCAL CMD_DESC cmd[] =
        {
            {
                "ver",
                MODULE_CLI,
                CMD_SYS_VER,
                true,
                [](ArgcList &v) -> void
                {
                    v.add("", 0, "show system version");
                },

            },
            {
                "help",
                MODULE_CLI,
                CMD_SYS_HELP,
                true,
                [](ArgcList &v) -> void
                {
                    v.add<std::string>("cmd", 'h', "cmd help info", false);
                },
            },
            {

                "dump",
                MODULE_CLI,
                CMD_SYS_DUMP,
                true,
                [](ArgcList &v) -> void
                {
                    v.add<std::string>("module", 'm', "dump module info", false);
                },
            },
            {
                "exit",
                MODULE_CLI,
                CMD_SYS_EXIT,
                true,
                [](ArgcList &v) -> void
                {
                    v.add("", 0, "exits the current user");
                },

            },
#ifdef _DEBUG
            {
                "sys-exit",
                MODULE_CLI,
                CMD_SYS_SHUT_DOWN,
                true,
                [](ArgcList &v) -> void
                {
                    v.add("", 0, "shutdown system");
                },

            },
#endif
            {
                "get-err-list",
                MODULE_CLI,
                CMD_SYS_ERR_CODE,
                true,
                [](ArgcList &v) -> void
                {
                    v.add("", 0,
                          "Get all the error code information registered by the system");
                },

            },
            {
                "get-key-list",
                MODULE_CLI,
                CMD_SYS_KEY,
                true,
                [](ArgcList &v) -> void
                {
                    v.add("", 0,
                          "Get all the keyword information registered by the system");
                },

            },
            {
                "set-cmd-more-tips",
                MODULE_CLI,
                CMD_MORE_TIPS,
                true,
                [](ArgcList &v) -> void
                {
                    v.add<std::string>("status", 's',
                                       "Prompt more when using wrong command", true, "off",
                                       parser::oneof<std::string>("off", "on"));
                },
            },
        };
    (void)RegCmd(cmd, ARRAY_SIZE(cmd));

    return 0;
}
REG_TO_FRAMEWORK(TABLE_ONE, MODULE_KERNEL, cliMgr, MODULE_CLI)