#include <objKernel.h>
#include <cnotify.h>
#include <system.h>
static const BYTE Major_Version_Number = 1; // .Minor_Version_Number[.Revision_Number[.Build_Number]]
static const BYTE Minor_Version_Number = 3;
static const BYTE Revision_Number = 8;

objKernel *g_objKernel;

objKernel::objKernel()
{
    Welcome();
    g_objKernel = this;
    m_objList["objKernel"] =
        std::make_shared<ObjModule>((objbase *)(this),
                                    "objKernel", MODULE_KERNEL);
}
DWORD objKernel::VersionGet() const
{
    union
    {
        DWORD verVal;
        BYTE ver[4];
    } i_u;
    i_u.ver[0] = Major_Version_Number;
    i_u.ver[1] = Minor_Version_Number;
    i_u.ver[2] = Revision_Number;
    //i_u.ver[3] = Build_Number;
    return i_u.verVal;
}
void objKernel::Welcome(int fd)
{
    std::ostringstream oss;

    oss << std::endl
        << "    ____  __________  ____" << std::endl
        << "   / __ \\/ ____/ __ \\/ __ \\" << std::endl
        << "  / / / / /   / / / / /_/ /" << std::endl
        << " / /_/ / /___/ /_/ / ____/" << std::endl
        << "/_____/\\____/\\____/_/" << std::endl
        << std::endl
        << " Herobrine (Alpha) V"
        << std::to_string(Major_Version_Number)
        << "." << std::to_string(Minor_Version_Number)
        << "." << std::to_string(Revision_Number) << std::endl
        << " Update Date: " << __DATE__ << std::endl
        << " Update Time: " << __TIME__ << std::endl
        << std::endl;
    LVOS_Write(fd, oss);
}
objKernel::~objKernel()
{
    Rspmsg rspmsg;
    Parameter list = std::make_shared<ARGC>();
    Cnotify *pnotify =
        reinterpret_cast<Cnotify *>(g_objKernel->InterFace(MODULE_NOTIFY));
    pnotify->NotifyA(list, rspmsg, MODULE_ALL, CMD_SYS_SHUT_DOWN, true);
    m_objList.clear();
}
objbase *objKernel::InterFace(const char *pzName)
{
    std::unique_lock<std::mutex> lock{this->m_lock};
    auto iter = OS::find(pzName, m_objList);
    if (iter != m_objList.end())
        return iter->second->obj;
    return nullptr;
}
objbase *objKernel::InterFace(int key)
{
    std::unique_lock<std::mutex> lock{this->m_lock};
    for (auto &iter : m_objList)
    {
        if (iter.second->id == key)
            return iter.second->obj;
    }
    return nullptr;
}
void objKernel::dump(TextTable &t)
{
    t.add("objId").add("objName").add("objPtr").add("refCount").endOfRow();
    t.setTableName("objKernel");

    for (auto &iter : m_objList)
    {
        t.add(std::to_hexstring(iter.second->id)).add(iter.first);
        t.add(std::to_hexstring(iter.second->obj));
        t.add(std::to_string(iter.second->refCount)).endOfRow();
    }
}
objbase *objKernel::Query(int key)
{
    std::unique_lock<std::mutex> lock{this->m_lock};
    for (auto &iter : m_objList)
    {
        if (iter.second->id == key)
        {
            iter.second->refCount++;
            return iter.second->obj;
        }
    }
    return nullptr;
}
void objKernel::Release(int key)
{
    std::unique_lock<std::mutex> lock{this->m_lock};
    for (auto &iter : m_objList)
    {
        if ((iter.second->id == key) && (iter.second->refCount > 0))
            iter.second->refCount--;
    }
}
void objKernel::Entry()
{
    UINT64 entrytime = 0;
    UINT64 endtime = 0;
    entrytime = GetCurrentMillisecs();

    // initial log config
    logInitial();

    // initial errcode
    ::ErroReg(nullptr, 0);
    ::KeyReg(nullptr, 0);

    // initial module initial callback
    FRAMEWORK_INIT([](FrameWork *arg)
                   {
                       objbase *pObj = g_objKernel->InterFace(arg->parentId);
                       if (pObj != nullptr)
                       {
                           pObj->Reg(arg->name, arg->objPtr, arg->id);
                       }
                   });
    // per-initial all module
    for (auto &iter : m_objList)
    {
        iter.second->obj->preInit();
    }

    // initial all module
    for (auto &iter : m_objList)
    {
        if (iter.second->obj->postInit())
        {
            LVOS_Printf(0,
                        "FATAL --->>>> init moudle:%s is failed(%s).\r\n",
                        iter.second->name, strerror(errno));
            return;
        }
    }
    endtime = GetCurrentMillisecs();
    LOG_INFO("System initialization completion time(%llums)",
             endtime - entrytime);

    /* 开始循环处理 */
    if (m_EntryFunc)
        m_EntryFunc();
    // free
    logRelease();
}
void objKernel::Init(VoidEntryFunc EntryFunc)
{
    if (EntryFunc)
        m_EntryFunc = EntryFunc;
}

void objKernel::Reg(const char *pzName, void *obj, int id)
{
    std::unique_lock<std::mutex> lock{this->m_lock};
    m_objList[pzName] =
        std::make_shared<ObjModule>((objbase *)obj, pzName, id);
}
STATUS objKernel::UnReg(int id)
{
    std::unique_lock<std::mutex> lock{this->m_lock};
    for (auto iter = m_objList.begin(); iter != m_objList.end(); iter++)
    {
        if (iter->second->id == id)
        {
            if (iter->second->refCount == 0)
            {
                m_objList.erase(iter);
                break;
            }
            else
                return ERR_MODULE_REF_CNT;
        }
    }
    return SUCCESS;
}