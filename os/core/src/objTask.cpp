#include <objTask.h>
#include <cnotify.h>
#include <timer.h>
#include <system.h>
objTaskMgr::objTaskMgr() {}
objTaskMgr::~objTaskMgr()
{
    UINT64 entrytime = GetCurrentMillisecs();

    while (1)
    {
        {
            std::unique_lock<std::mutex> lock{this->m_lock};
            if (m_objlist.size() == 0)
            {
                break;
            }
            else
            {
                UINT64 endtime = GetCurrentMillisecs();
                if ((endtime - entrytime) > 5 * 1000)
                {
                    LOG_ERROR("Task wating begin time %llu endtime %llu", entrytime, endtime);
                    for (auto &iter : m_objlist)
                    {
                        LOG_FATAL("wating for %s(%0X) exit timeout.",
                                  iter.second->Name(), iter.second->GetId());
                    }
                }
            }
        }
    }
}
void objTaskMgr::dump(TextTable &t)
{
    t.add("taskName").add("taskId").add("objPtr").endOfRow().setTableName("objTaskMgr");

    for (auto &iter : m_objlist)
    {
        t.add(iter.second->Name()).add(std::to_hexstring(iter.second->GetId()));
        t.add(std::to_hexstring(iter.second)).endOfRow();
    }
}
int objTaskMgr::addObj(std::shared_ptr<objTask> obj)
{
    std::unique_lock<std::mutex> lock{this->m_lock};
    auto iter = m_objlist.find(obj->GetId());
    if ((iter != m_objlist.end()) && (m_objlist.size() != 0))
        return RET_ERR;
    m_objlist[obj->GetId()] = obj;
    return RET_OK;
}
void objTaskMgr::delObj(UINT64 id)
{
    std::unique_lock<std::mutex> lock{this->m_lock};
    auto iter = m_objlist.find(id);
    if (iter != m_objlist.end())
    {
        m_objlist.erase(iter);
    }
}

STATUS objTaskEntry(const char *objTaskName,
                    FunEntry func, void *arg,
                    const char *file,
                    int len)
{
    auto obj = std::make_shared<objTask>(objTaskName, func, arg);
    if (obj == nullptr)
        return ERROR;
    auto callfunc = [obj]()
    {
        std::thread::id id = std::this_thread::get_id();
        objTaskMgr *taskmgr = reinterpret_cast<objTaskMgr *>(
            g_objKernel->InterFace(MODULE_TASK));
        obj->SetId((*(UINT *)(&id)));
        taskmgr->addObj(obj);
        obj->Run();
        taskmgr->delObj((*(UINT *)(&id)));
        LOG_TRACE("Destory task: %s(%#0x)", obj->Name(), obj->GetId());
    };
    std::thread task(callfunc);
    task.detach();
    LOG_TRACE("Create task: %s(%#0x)", objTaskName, obj);
    return RET_OK;
}

objTask::objTask(const char *pzName, FunEntry func, void *obj)
{
    if (pzName != nullptr)
        strncpy_s(name, OBJ_NAME_MAX_LEN, pzName, OBJ_NAME_MAX_LEN - 1);
    name[OBJ_NAME_MAX_LEN - 1] = '\0';
    m_func = func;
    m_objPara = obj;
}
objTask::~objTask() {}

REG_TO_FRAMEWORK(TABLE_ONE, MODULE_KERNEL, objTaskMgr, MODULE_TASK)

int workQ::postInit()
{
    Cnotify *obj =
        reinterpret_cast<Cnotify *>(g_objKernel->InterFace(MODULE_NOTIFY));
    if (obj)
        obj->RegReceiver(MODULE_WORK_TASK, this, "workQ");
    CREATE_OBJTASK(
        "WorkQueue", [=](void *pobjPara)
        { this->Proc(); },
        nullptr);
    return SUCCESS;
}
void workQ::Proc()
{
    while (!this->stoped)
    {
        Task task;
        {
            std::unique_lock<std::mutex> lock{this->m_lock};

            this->cv_task.wait(lock,
                               [this]
                               {
                                   return this->stoped.load() ||
                                          !this->tasks.empty();
                               });
            if (this->stoped && this->tasks.empty())
            {
                return;
            }
            task = std::move(this->tasks.front());
            this->tasks.pop();
        }
        task();
    }
}
workQ::workQ()
{
}
workQ::~workQ()
{
    stoped.store(true);
    cv_task.notify_all();
}
void workQ::dump(TextTable &t)
{
    t.add("Queue size").add("Queue status").endOfRow().setTableName("objKernel");
    t.add(std::to_string(tasks.size())).add((stoped ? "Stop" : "Run")).endOfRow();
}
int workQ::Process(ArgcList &message, Rspmsg &outmessage,
                   int iModule, int iCmd)
{
    PROCESS_BEGIN(iCmd)
    PROCESS_CALL(CMD_SYS_SHUT_DOWN, sysShutdown)
    PROCESS_END()
}
int workQ::sysShutdown(ArgcList &message, Rspmsg &outmessage,
                       int iModule, int iCmd)
{
    stoped.store(true);
    cv_task.notify_all();
    return SUCCESS;
}
STATUS workQ::commit(Task job)
{
    if (stoped.load())
        return ERROR;
    std::lock_guard<std::mutex> lock{m_lock};
    tasks.emplace(job);
    cv_task.notify_one();
    return SUCCESS;
}
REG_TO_FRAMEWORK(TABLE_ONE, MODULE_KERNEL, workQ, MODULE_WORK_TASK)

STATUS jobAdd(VoidEntryFunc fun)
{
    return reinterpret_cast<workQ *>(
               g_objKernel->InterFace(MODULE_WORK_TASK))
        ->commit(fun);
}