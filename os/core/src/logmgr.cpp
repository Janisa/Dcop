#include <stacktrace.h>
#include <clibase.h>
#include <table.h>

class LogMgr : public objbase
{
private:
    int GetLevelByLog(ArgcList &message,
                      Rspmsg &outmessage,
                      int iModule,
                      int iCmd)
    {

        LogFile *logPtr = logModuleGet(ValueGet(message.get<std::string>("module")));
        if (logPtr == nullptr)
            return ERR_MODULE_NOT_EXIST;
        std::map<std::string, std::string> retVal;
        retVal["module"] = KeyByNameGet(message.get<std::string>("module"));
        retVal["level"] = KeyGet(logPtr->getLevel());
        outmessage.push_back(retVal);
        return RET_OK;
    }
    int SetLogByLevel(ArgcList &message, Rspmsg &outmessage,
                      int iModule, int iCmd)
    {

        LogFile *logPtr = logModuleGet(ValueGet(message.get<std::string>("module")));
        if (logPtr == nullptr)
            return ERR_MODULE_NOT_EXIST;

        logPtr->setLevel(ValueGet(message.get<std::string>("level")));
        return RET_OK;
    }

public:
    int Process(ArgcList &message, Rspmsg &outmessage, int iModule, int iCmd)
    {
        PROCESS_BEGIN(iCmd)
        PROCESS_CALL(CMD_SET_LOG_TRACE, SetLogTreace)
        PROCESS_CALL(CMD_GET_LOG_TRACE, GetLogTreace)
        PROCESS_CALL(CMD_GET_LOG_LEVEL, GetLevelByLog)
        PROCESS_CALL(CMD_SET_LOG_LEVEL, SetLogByLevel)
        PROCESS_END()
    }
    int GetLogTreace(ArgcList &message, Rspmsg &outmessage,
                     int iModule, int iCmd)
    {
        LogFile *logPtr = logModuleGet(ValueGet(message.get<std::string>("module")));
        if (logPtr == nullptr)
            return ERR_MODULE_NOT_EXIST;

        std::map<std::string, std::string> retVal;
        retVal["module"] = KeyByNameGet(message.get<std::string>("module"));
        retVal["status"] = KeyGet(logPtr->GetDebug());
        outmessage.push_back(retVal);

        return RET_OK;
    }

    int SetLogTreace(ArgcList &message, Rspmsg &outmessage,
                     int iModule, int iCmd)
    {
        LogFile *logPtr = logModuleGet(ValueGet(message.get<std::string>("module")));
        if (logPtr == nullptr)
            return ERR_MODULE_NOT_EXIST;
        logPtr->SetDebug(ValueGet(message.get<std::string>("status")));

        return SUCCESS;
    }

public:
    int postInit()
    {
        LOCAL CMD_DESC cmd[] =
            {
                {
                    "set-log-level",
                    MODULE_LOG_MGR,
                    CMD_SET_LOG_LEVEL,
                    true,
                    [](ArgcList &v) -> void
                    {
                        v.add<std::string>("module", 'm', "module name", true, "kernel")
                            .add<std::string>("level", 'l',
                                              "set log level to module", true, "info",
                                              parser::oneof<std::string>("trace", "debug", "info", "warn", "error", "fatal"));
                    },
                },
                {

                    "get-log-level",
                    MODULE_LOG_MGR,
                    CMD_GET_LOG_LEVEL,
                    true,
                    [](ArgcList &v) -> void
                    {
                        v.add<std::string>("module", 'm', "module name", true, "kernel");
                    },
                },
                {
                    "set-log-trace",
                    MODULE_LOG_MGR,
                    CMD_SET_LOG_TRACE,
                    true,
                    [](ArgcList &v) -> void
                    {
                        v.add<std::string>("module", 'm', "module name", true, "kernel")
                            .add<std::string>("status", 's',
                                              "set log level to module", true, "off",
                                              parser::oneof<std::string>("off", "on"));
                    },
                },
                {
                    "get-log-trace",
                    MODULE_LOG_MGR,
                    CMD_GET_LOG_TRACE,
                    true,
                    [](ArgcList &v) -> void
                    {
                        v.add<std::string>("module", 'm', "module name", true, "kernel");
                    },
                },
            };
        reinterpret_cast<Cnotify *>(
            g_objKernel->InterFace(MODULE_NOTIFY))
            ->RegReceiver(
                MODULE_LOG_MGR, this, "LogMgr");
        reinterpret_cast<cliMgr *>(
            g_objKernel->InterFace(MODULE_CLI))
            ->RegCmd(cmd, ARRAY_SIZE(cmd));
        return 0;
    }
    LogMgr()
    {
    }
    ~LogMgr()
    {
    }
};

REG_TO_FRAMEWORK(TABLE_ONE, MODULE_KERNEL, LogMgr, MODULE_LOG_MGR)