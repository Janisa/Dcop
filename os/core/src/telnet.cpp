﻿#include <telnet.h>
#include <objTask.h>
#define MAX_BUFFER_LEN 256
static Telnet *gObjTelnet = nullptr;
Telnet::Telnet(INT16 port)
{
#ifndef _MSC_VER
    m_port = port;
    m_listenFd = 0;
    m_isExit = EXIT_INVALID;
    m_epfd = 0;
    m_servList.clear();
#endif
    gObjTelnet = this;
}
Telnet::~Telnet()
{
#ifndef _MSC_VER
    close(m_listenFd);
    close(m_epfd);
    m_isExit = EXIT_INVALID;
    for (auto &iter : m_servList)
    {
        close(iter.first);
    }
#endif
}

void Telnet::PostMsg(std::ostringstream &oss)
{
#ifndef _MSC_VER
    for (auto &iter : m_servList)
        LVOS_Write(iter.first, oss);
#else
    LVOS_Write(0, oss);
#endif
}

#define MAX_EVENTS 100
int Telnet::postInit()
{
#if !defined(_MSC_VER) && !defined(VS_LINUX_DBG)
    struct sockaddr_in addr;
    int addrlen;

    m_listenFd = socket(PF_INET, SOCK_STREAM, 0);
    if (m_listenFd < 0)
    {
        LOG_FATAL("create socket error");
        return RET_ERR;
    }
    fcntl(m_listenFd, F_SETFL, O_NONBLOCK);

    addrlen = 1;
    if (setsockopt(m_listenFd, SOL_SOCKET, SO_REUSEADDR, &addrlen,
                   sizeof(addrlen)) != 0)
    {
        LOG_FATAL("setsockopt fail.");
        return RET_ERR;
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(m_port);
    addr.sin_addr.s_addr = INADDR_ANY;
    addrlen = sizeof(struct sockaddr_in);

    if (bind(m_listenFd, (struct sockaddr *)&addr, addrlen) < 0)
    {
        LOG_FATAL("bind socket error");
        return RET_ERR;
    }

    if (listen(m_listenFd, 5) < 0)
    {
        LOG_FATAL("listen socket error");
        return RET_ERR;
    }
    struct epoll_event ev;

    m_epfd = epoll_create(MAX_EVENTS);

    ev.data.fd = m_listenFd;

    ev.events = EPOLLIN | EPOLLET;

    if (epoll_ctl(m_epfd, EPOLL_CTL_ADD, m_listenFd, &ev) < 0)
    {
        LOG_FATAL("worker epoll_ctl error = %s.", strerror(errno));
        return RET_ERR;
    }
#endif
    m_cliMgr =
        reinterpret_cast<cliMgr *>(g_objKernel->Query(MODULE_CLI));
    g_objKernel->Init([this](){ this->Process(); });
    return RET_OK;
}

void Telnet::Process()
{
    int isExit = false;
#if defined(_MSC_VER) || defined(VS_LINUX_DBG)
    m_cliMgr->Welcome();
    while (!isExit)
    {
        char name[MAX_BUFFER_LEN] = { 0 };
        isExit = false;
        std::cin.getline(name, MAX_BUFFER_LEN);
        m_cliMgr->Process(0, name, strlen(name), &isExit);
    }
#else
    LONG len;
    struct epoll_event ev, events[MAX_EVENTS];
    while (!m_isExit)
    {
        int nfds = epoll_wait(m_epfd, events, MAX_EVENTS, -1);

        for (int i = 0; i < nfds; ++i)
        {
            if (events[i].data.fd == m_listenFd)
            {
                socklen_t clilen;
                struct sockaddr_in clientaddr;

                int sockfd = accept(m_listenFd,
                                    (sockaddr *)&clientaddr,
                                    &clilen);
                if (sockfd < 0)
                {
                    continue;
                }

                if (fcntl(sockfd,
                          F_SETFL,
                          fcntl(sockfd, F_GETFD, 0) | O_NONBLOCK) == -1)
                {
                    continue;
                }
                ev.data.fd = sockfd;
                ev.events = EPOLLIN | EPOLLET;

                epoll_ctl(m_epfd, EPOLL_CTL_ADD, sockfd, &ev);
                m_cliMgr->Welcome(sockfd);
                {
                    std::unique_lock<std::mutex> lock{m_synclock};
                    m_servList[sockfd] = sockfd;
                }
            }
            else if (events[i].events & EPOLLIN)
            {
                int sockfd = events[i].data.fd;
                if (sockfd < 0)
                {
                    continue;
                }

                char buf[MAX_BUFFER_LEN] = {0};

                bzero(buf, sizeof(buf));
                len = read(sockfd, buf, MAX_BUFFER_LEN - 1);

                if (len <= 0)
                {
                    close(sockfd);
                    events[i].data.fd = -1;
                    {
                        std::unique_lock<std::mutex> lock{m_synclock};
                        m_servList.erase(sockfd);
                    }

                    ev.data.fd = sockfd;
                    epoll_ctl(m_epfd, EPOLL_CTL_DEL, sockfd, &ev);
                    LOG_ERROR("worker read data error = %s.",
                              strerror(errno));
                }
                else
                {
                    char *pos = strrchr(buf, '\r');
                    isExit = EXIT_INVALID;
                    buf[len - 1] = '\0';
                    if (pos != nullptr)
                    {
                        *pos = '\0';
                    }
                    m_cliMgr->Process(sockfd, buf,
                                      strlen(buf), &isExit);
                    switch (isExit)
                    {
                    case USER_EXIT:
                        close(sockfd);
                        events[i].data.fd = -1;
                        ev.data.fd = sockfd;
                        epoll_ctl(m_epfd, EPOLL_CTL_DEL, sockfd, &ev);
                        break;
                    case SYS_EXIT:
                        return;
                    default:
                        break;
                    }
                }
            }
        }
    }
#endif
}
int Report(Rspmsg &outMessage, TextTable &t)
{
    if (outMessage.size() <= 0)
    {
        return ERROR;
    }
    for (auto &itv : *outMessage.begin())
    {
        t.add(itv.first);
    }
    t.endOfRow();
    for (auto iter = outMessage.begin();
         iter != outMessage.end(); iter++)
    {
        for (auto &itv : *iter)
        {
            t.add(itv.second);
        }
        t.endOfRow();
    }
    return RET_OK;
}
int Report(Rspmsg &outMessage)
{
    TextTable t(outMessage.getName());
    if (RET_OK != Report(outMessage, t))
    {
        return RET_ERR;
    }
    std::ostringstream oss;
    oss << t;
    gObjTelnet->PostMsg(oss);
    return RET_OK;
}
REG_TO_FRAMEWORK(TABLE_ONE, MODULE_KERNEL, Telnet, MODULE_TELNET)