#include <log.h>
#include <stacktrace.h>
#include <system.h>

#define LOG_MESSAGE_LEN 1024
#define LOG_PATH "../log/"

#ifndef _MSC_VER
#define SPLIT '/'
#else
#define SPLIT '\\'
#endif
#define NEWLINE "\n"
#define DEFAULT_LOG_FILE_PATH "../log/message"

#ifdef LOG_ASSERT_ENABLE
void (*log_assert_hook)(const char *expr, const char *func, size_t line);
#endif

LogFile::LogFile(const char *file, int fileCnt, int fileSize, int level)
{
    strncpy_s(m_fileName, LOG_FILENAME_LEN - 1, file, LOG_FILENAME_LEN);
    m_fileCnt = fileCnt;
    m_fileSzie = fileSize * (1024 * 1024); // MB
    m_logLevel = level;
    mkdirPath(LOG_PATH);
    debug_backtrace_init();
    m_file = nullptr;
    reOpen();
    SetDebug(KEY_VAL_OFF);
}
LogFile::~LogFile()
{
    std::lock_guard<std::mutex> lock{m_lock};
    if (m_file)
        fclose(m_file);
    m_file = nullptr;
}
int LogFile::write(int level, const char *file, const char *func,
                   int line, const char *format, ...)
{
    int len = 0;
    va_list arg_ptr;
    va_start(arg_ptr, format);
    len = write(level, file, func, line, format, arg_ptr);
    va_end(arg_ptr);

    return len;
}
int LogFile::compress()
{
#ifndef _MSC_VER
    char cmdbuf[CMD_BUFFER_LEN] = {0};
    char tmbuf[CMD_BUFFER_LEN] = {0};
    char readNum[16] = {0};
    struct tm ptm;
    time_t t = time(NULL);

    (void)snprintf_s(cmdbuf, CMD_BUFFER_LEN, CMD_BUFFER_LEN - 1,
                     "ls %s.* | wc -l", m_fileName);
    if (OS_GetStrValueByCmd(cmdbuf, readNum, sizeof(readNum)) != RET_OK)
        return RET_ERR;

    if (std::to_int<int>(std::string(readNum)) != m_fileCnt)
        return RET_ERR;

#ifndef _MSC_VER
    localtime_r(&t, &ptm);
#else
    localtime_s(&ptm, &t);
#endif

    (void)snprintf_s(tmbuf, LOG_MESSAGE_LEN, LOG_MESSAGE_LEN - 1,
                     "%4d%02d%02d%02d%02d%02d",
                     ptm.tm_year + 1900, ptm.tm_mon + 1,
                     ptm.tm_mday, ptm.tm_hour,
                     ptm.tm_min, ptm.tm_sec);
    (void)snprintf_s(cmdbuf, CMD_BUFFER_LEN, CMD_BUFFER_LEN - 1,
                     "tar -zcvf %s%s.tgz %s.* --remove-files >/dev/null 2>&1",
                     m_fileName,
                     tmbuf,
                     m_fileName);
    return OS_SafeSystem(cmdbuf, NULL, OM_CMD_EXCE_TIME, NULL);
#else
    return RET_OK;
#endif
}
int LogFile::write(int level, const char *file, const char *func,
                   int line, const char *format, va_list argList)
{
    char log_msg[LOG_MESSAGE_LEN] = {0};
    struct tm ptm;
    time_t t = time(NULL);
    int nWrittenBytes = 0;
    level = level & LOG_LEVEL_MASK;

    PARAM_COND_CHECK_RET_VAL(level > m_logLevel, -1);

#ifndef _MSC_VER
    localtime_r(&t, &ptm);
#else
    localtime_s(&ptm, &t);
#endif
#define WRITE_FORMAT_POS (log_msg + nWrittenBytes)
#define WRITE_LAVE_LEN   (LOG_MESSAGE_LEN - nWrittenBytes)

    nWrittenBytes = snprintf_s(WRITE_FORMAT_POS,
                               WRITE_LAVE_LEN,
                               WRITE_LAVE_LEN - 1,
                               "%4d-%02d-%02d %02d:%02d:%02d",
                               ptm.tm_year + 1900,
                               ptm.tm_mon + 1,
                               ptm.tm_mday,
                               ptm.tm_hour,
                               ptm.tm_min,
                               ptm.tm_sec);

    const char *splitpos = strrchr(file, SPLIT);
    nWrittenBytes += snprintf_s(WRITE_FORMAT_POS,
                                WRITE_LAVE_LEN,
                                WRITE_LAVE_LEN - 1,
                                " [%-5s] [%s:%d] [%s] ",
                                KeyGet(level).c_str(),
                                (NULL != splitpos) ? (splitpos + 1) : file,
                                line, func);

    nWrittenBytes += vsnprintf_s(WRITE_FORMAT_POS, WRITE_LAVE_LEN - 1, WRITE_LAVE_LEN, format, argList);

    if (log_msg[nWrittenBytes] != '\n')
    {
        nWrittenBytes += snprintf_s(WRITE_FORMAT_POS, WRITE_LAVE_LEN, WRITE_LAVE_LEN - 1, "%s", NEWLINE);
    }

    return write(log_msg, nWrittenBytes);
}
/*
 * rotate the log file xxx.log.n-1 => xxx.log.n, and xxx.log => xxx.log.0
 */
void LogFile::file_rotate(void)
{
#define SUFFIX_LEN (LOG_FILENAME_LEN + 10)
    /* mv xxx.log.n-1 => xxx.log.n, and xxx.log => xxx.log.0 */
    int n;
    char oldpath[SUFFIX_LEN], newpath[SUFFIX_LEN];
    size_t base = strlen(m_fileName);

    memcpy_s(oldpath, SUFFIX_LEN, m_fileName, base);
    memcpy_s(newpath, SUFFIX_LEN, m_fileName, base);

    for (n = m_fileCnt - 1; n >= 0; --n)
    {
        snprintf(oldpath + base, SUFFIX_LEN, n ? ".%d" : "", n - 1);
        snprintf(newpath + base, SUFFIX_LEN, ".%d", n);
        remove(newpath);
        rename(oldpath, newpath);
    }
}

/*
 * Check if it needed retate
 */
bool LogFile::file_retate_check(void)
{
    struct stat statbuf;
    statbuf.st_size = 0;
    if (stat(m_fileName, &statbuf) < 0)
    {
        reOpen();
        return false;
    }

    PARAM_COND_CHECK_RET_VAL(statbuf.st_size > m_fileSzie, true);

    return false;
}
inline void LogFile::close()
{
    std::lock_guard<std::mutex> lock{m_lock};
    if (m_file)
        fclose(m_file);
    m_file = nullptr;
}
inline void LogFile::reOpen()
{
    close();
    m_file = fopen(m_fileName, "at+");
    setvbuf(m_file, NULL, _IOLBF, 2);
}
int LogFile::write(const char *buf, int len)
{
    int ret = ERROR;

    {
        std::lock_guard<std::mutex> lock{m_lock};
        if (m_file)
        {
            ret = fwrite(buf, 1, len, m_file);
        }
    }

    if (ret <= 0)
    {
        mkdirPath(LOG_PATH);
        reOpen();
    }
    else if (file_retate_check())
    {
        close();
        {
            std::lock_guard<std::mutex> lock{m_synclock};
            file_rotate();
            compress();
        }
        reOpen();
    }
    else
    {
    }

    if (GetDebug() == KEY_VAL_ON)
        std::cout << buf;

    return ret;
}
/**
 * dump the hex format data to log
 *
 * @param name name for hex object, it will show on log header
 * @param width hex number for every line, such as: 16, 32
 * @param buf hex buffer
 * @param size buffer size
 */
void LogFile::hexdump(const char *name, uint8_t width,
                      const char *buf, uint16_t size)
{
#define __is_print(ch) ((unsigned int)((ch) - ' ') < 127u - ' ')
#define _HEX_LEN 4
#define _MAX_WDT 32

    int idxLine, idxChar;
    char dumpName[_MAX_WDT + 1] = {0};

    width = MIN(_MAX_WDT, width);
    width = MAX(_MAX_WDT, width);
    (void)strncpy(dumpName, name, _MAX_WDT);

    for (idxLine = 0; idxLine < size; idxLine += width)
    {
        char hexstring[_HEX_LEN] = {' ', ' ', ' ', 0};
        int fmt_result = 0;
        char dump_string[LOG_MESSAGE_LEN + 1] = {0};
        char visible_string[_MAX_WDT + 1] = {0};
        char curChar = 0;

#define LAVE_LEN    (LOG_MESSAGE_LEN - fmt_result)
#define FORMAT_POS  (dump_string + fmt_result)
#define RESERVE_LEN (LOG_MESSAGE_LEN - (width * 4 + 2))
#define READ_IDX    (idxLine + idxChar)
#define WRITE_IDX   (visible_string + idxChar)

        /* package header */

        fmt_result = snprintf(FORMAT_POS, RESERVE_LEN,
                              "HEX %s: %04X-%04X: ",
                              dumpName, idxLine, idxLine + width - 1);

        /* dump hex */
        for (idxChar = 0; idxChar < width; idxChar++)
        {
            curChar = buf[READ_IDX];
            if (READ_IDX < size)
            {
                (void)snprintf(hexstring, _HEX_LEN, "%02X ", curChar);
                (void)snprintf(WRITE_IDX, _MAX_WDT, "%c",
                               __is_print(curChar) ? curChar : '.');
            }
            fmt_result += snprintf(FORMAT_POS, LAVE_LEN, "%s", hexstring);
        }

        fmt_result += snprintf(FORMAT_POS, LAVE_LEN, "  %s" NEWLINE, visible_string);

        write(dump_string, fmt_result);
    }
}
int mkdirPath(const char *path)
{
#ifndef _MSC_VER
    char currentdir[MAX_PATH] = {0};
    char *buffer = getcwd(NULL, 0);

    PARAM_COND_CHECK_RET_VAL(buffer == NULL, -1);

    (void)snprintf_s(currentdir, MAX_PATH, MAX_PATH - 1,
                     "%s/%s", buffer, path);
    free(buffer);
    return mkdir(currentdir, 0777);
#else
    return (_mkdir(path) == 0);
#endif
}

static std::map<UINT, LogFile *> logModuleList;

LogFile *logModuleGet(UINT module)
{
    auto logInst = logModuleList.find(module);

    PARAM_COND_CHECK_RET_VAL(logInst != logModuleList.end(), logInst->second);

    return nullptr;
}

int logWrite(UINT module, int level, const char *file, const char *func,
             int line, const char *format, ...)
{
    LogFile *logInst = logModuleGet(module);

    PARAM_COND_CHECK_RET_VAL(logInst == nullptr, 0);

    int len = 0;
    va_list arg_ptr;
    va_start(arg_ptr, format);
    len = logInst->write(level, file, func, line, format, arg_ptr);
    va_end(arg_ptr);

    return len;
}
void logHexdump(UINT module, const char *name, uint8_t width,
                const char *buf, uint16_t size)
{
    LogFile *logInst = logModuleGet(module);

    PARAM_COND_CHECK_RET_VAL(logInst == nullptr, );

    logInst->hexdump(name, width, buf, size);
}

int logInitial()
{
    logModuleList.clear();

    return logModuleReg (DEFAULT_LOG_FILE_PATH) ;
}

void logRelease()
{
    for (auto &iter : logModuleList)
    {
        if (iter.second)
            free(iter.second);
    }
    logModuleList.clear();
}

int logModuleReg(const char *file, int fileCnt,
                 int fileSize, int level)
{
    LogFile *logInst = new LogFile(file, fileCnt, fileSize, level);

    PARAM_COND_CHECK_RET_VAL(logInst == nullptr, ERROR);

    logModuleList[logModuleList.size()] = logInst;

    return logModuleList.size() - 1;
}
