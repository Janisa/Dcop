#include <cmdid.h>
#include <base.h>
#include <logFile.h>

static ERR_CODE_INFO gErrCode[] = {
    {ERROR,        "ERROR",   "Operation failure"},
    {SUCCESS,      "SUCCESS", "Operation is successful"},
    {ERR_DISPATCH, "ERR_DISPATCH", "Command distribution failed"},
    {ERR_NOTIFY, "ERR_NOTIFY", "Message publishing failed"},
    {ERR_REPORT, "ERR_REPORT", "Failed to report the message"},
    {ERR_PARAM,  "ERR_PARAM",  "Parameter processing error"},
    {ERR_UNKOWN, "ERR_UNKOWN", "Unregistered error"},
    {ERR_NOT_SUPPORT, "ERR_NOT_SUPPORT", "Feature not supported"},
    {ERR_MSG_PARAM, "ERR_MSG_PARAM", "Message parameter is invalid"},
    {ERR_MSG_FIELD, "ERR_MSG_FIELD", "Invalid message field"},
    {ERR_MODULE_REF_CNT,   "ERR_MODULE_REF_CNT",   "Module has references"},
    {ERR_MODULE_NOT_EXIST, "ERR_MODULE_NOT_EXIST", "Module does not exist or registered"},
};

static KEY_MAP gKeyMap[] = {
    {"true",    true,            KEY_VAL_TRUE},
    {"false",   false,           KEY_VAL_FALSE},
    {"off",     KEY_VAL_OFF,     KEY_VAL_OFF},
    {"on",      KEY_VAL_ON,      KEY_VAL_ON},
    {"default", KEY_VAL_INVALID, KEY_VAL_INVALID},
    {"FATAL",   LOG_LVL_FATAL,   KEY_VAL_FATAL},
    {"ERROR",   LOG_LVL_ERROR,   KEY_VAL_ERROR},
    {"WARN",    LOG_LVL_WARN,    KEY_VAL_WARN},
    {"INFO",    LOG_LVL_INFO,    KEY_VAL_INFO},
    {"DEBUG",   LOG_LVL_DEBUG,   KEY_VAL_DEBUG},
    {"TRACE",   LOG_LVL_TRACE,   KEY_VAL_TRACE},
    {"cli",     1,               KEY_VAL_CLI},
    {"kernel",  0,               KEY_VAL_KERNEL},
};
std::map<int, ERR_CODE_INFO *> m_errList;
std::map<std::string, KEY_MAP*> m_keyList;
static std::mutex m_lock;

void ErroReg(ERR_CODE_INFO *err, int count)
{
    static bool first = true;
    if (first)
    {
        first = false;
        ErroReg(gErrCode, ARRAY_SIZE(gErrCode));
    }
    std::unique_lock<std::mutex> lock{m_lock};
    for (int i = 0; i < count; i++)
    {
        auto iter = m_errList.find(err[i].id);
        if (iter != m_errList.end())
            continue;
        m_errList[err[i].id] = &err[i];
    }
}
ERR_CODE_INFO *ErrGet(int err)
{
    auto iter = m_errList.find(err);
    if (iter != m_errList.end())
        return iter->second;
    return m_errList[ERR_UNKOWN];
}
void KeyReg(KEY_MAP *key, int count)
{
    static bool first = true;
    if (first)
    {
        first = false;
        KeyReg(gKeyMap, ARRAY_SIZE(gKeyMap));
    }
    std::unique_lock<std::mutex> lock{m_lock};
    for (int i = 0; i < count; i++)
    {
        auto iter = m_keyList.find(key[i].key);
        if (iter != m_keyList.end())
            continue;
        std::string ketstr(key[i].key);
        std::transform(ketstr.begin(), ketstr.end(),
                       ketstr.begin(), ::toupper);
        m_keyList[ketstr] = &key[i];
    }
}
static inline std::map<std::string, KEY_MAP *>::iterator _ValueGet(std::string &key)
{
    std::transform(key.begin(), key.end(),
                   key.begin(), ::toupper);
    return m_keyList.find(key);
}
int ValueGet(std::string key)
{
    auto iter = _ValueGet(key);
    if (iter != m_keyList.end())
        return iter->second->value;
    return m_keyList["DEFAULT"]->value;
}
int RevalueGet(std::string key)
{
    auto iter = _ValueGet(key);
    if (iter != m_keyList.end())
        return iter->second->reValue;
    return m_keyList["DEFAULT"]->reValue;
}

std::string KeyGet(int value)
{
    for (auto &key : m_keyList)
    {
        if (key.second->reValue == value)
            return key.first;
    }
    return "";
}
std::string KeyByReValueGet(int reValue)
{
    for (auto &key : m_keyList)
    {
        if (key.second->reValue == reValue)
            return key.first;
    }
    return "";
}
std::string KeyByNameGet(std::string name)
{
    auto iter = _ValueGet(name);
    if (iter != m_keyList.end())
        return iter->first;
    return "";
}